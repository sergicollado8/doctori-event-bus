# Event Bus

![source](https://img.shields.io/badge/source-doctori/event--bus-blue)
![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![php](https://img.shields.io/badge/php->=8.0-8892bf)

Event Bus models to use in Doctor I applications, you can use RabbitMQ, InMemory or MySQL.

## 🚀 Environment Setup

### 🛠️ Install dependencies
Install the dependencies if you haven't done it previously: `make build` or `composer install`

### ✅ Tests execution
Execute PHPUnit tests:
```bash
$ make run-test
```

### 🎨 Style
This project follows PSR-12 style
```bash
$ make check-style

$ make fix-style
```

### 🧐 Inspection
This project should be inspected by PHPStan
```bash
$ make inspect-phpstan
```

## 🔥 Usage

### 🛠️ Install in a third application
Include the package with composer.
```bash
$ composer require doctori/event-bus
```

Add the following lines on your service.yaml with the tag that you use

```yaml
imports:
    - { resource: ../vendor/doctori/event-bus/config/services.yaml }

services:
    _instanceof:
        DoctorI\Shared\EventBus\Domain\Bus\Event\DomainEventSubscriber:
            tags: [ 'doctori.domain_event_subscriber' ]

    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\DomainEventMapping:
        arguments: [ !tagged doctori.domain_event_subscriber ]

    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\DomainEventSubscriberLocator:
        arguments: [ !tagged doctori.domain_event_subscriber ]

    # InMemory
    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\InMemory\InMemorySymfonyEventBus:
        arguments: [ !tagged doctori.domain_event_subscriber ]
        lazy: true

    # RabbitMQ
    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection:
        arguments:
            - host: '%env(RABBITMQ_HOST)%'
              port: '%env(RABBITMQ_PORT)%'
              vhost: '%env(RABBITMQ_MOOC_VHOST)%'
              login: '%env(RABBITMQ_LOGIN)%'
              password: '%env(RABBITMQ_PASSWORD)%'
              read_timeout: 2
              write_timeout: 2
              connect_timeout: 5

    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqEventBus:
        arguments: [ '@DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection', '%env(RABBITMQ_EXCHANGE)%' ]

    DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqDomainEventsConsumer:
        arguments:
            - '@DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection'
            - '@DoctorI\Shared\EventBus\Infrastructure\Bus\Event\DomainEventJsonDeserializer'
            - '%env(RABBITMQ_EXCHANGE)%'
            - '%env(RABBITMQ_MAX_RETRIES)%'
```

And then if you want to use RabbitMQ, you should add this service:
```yaml
services:
    # Event bus
    DoctorI\Shared\EventBus\Domain\Bus\Event\EventBus: '@DoctorI\Shared\EventBus\Infrastructure\Bus\Event\RabbitMq\RabbitMqEventBus'
```

Or this one if you want to use InMemory
```yaml
services:
    # Event bus
    DoctorI\Shared\EventBus\Domain\Bus\Event\EventBus: '@DoctorI\Shared\EventBus\Infrastructure\Bus\Event\InMemory\InMemorySymfonyEventBus'
```
