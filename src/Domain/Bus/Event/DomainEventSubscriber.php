<?php

declare(strict_types=1);

namespace DoctorI\Shared\EventBus\Domain\Bus\Event;

interface DomainEventSubscriber
{
    public static function subscribedTo(): array;
}
