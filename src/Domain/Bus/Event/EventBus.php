<?php

declare(strict_types=1);

namespace DoctorI\Shared\EventBus\Domain\Bus\Event;

interface EventBus
{
    public function publish(DomainEvent ...$events): void;
}
